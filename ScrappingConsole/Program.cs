﻿// See https://aka.ms/new-console-template for more information

using System.Globalization;
using HtmlAgilityPack;

Console.WriteLine("Hello, World!");

var monedas = new[] { "dolar", "euro" };
var monedasArg = new[] { "Dolar U.S.A", "Euro" };
var culture = CultureInfo.CreateSpecificCulture("es");

var url = new Uri("http://www.bcv.org.ve/");
var htmldoc = new HtmlDocument();
using (var client = new HttpClient())
{
    var html = client.GetStringAsync(url).Result;
    htmldoc.LoadHtml(html);
}

//*[@id="dolar"]
foreach (var moneda in monedas)
{
    var nodo = htmldoc.DocumentNode.SelectSingleNode($"//div[@id=\"{moneda}\"]");
    var simbolo = nodo.SelectSingleNode("div/div/div[1]/span")?.InnerText;
    var precio = nodo.SelectSingleNode("div/div/div[2]")?.InnerText?.Trim();
    if (precio != null && decimal.TryParse(precio, NumberStyles.Any, culture, out var precioDec))
    {
        Console.WriteLine($"{simbolo} : {precioDec:##.#########}");
    }
}

var urlbna = new Uri("https://www.bna.com.ar/Personas");
using (var client = new HttpClient())
{
    var html = client.GetStringAsync(urlbna).Result;
    htmldoc.LoadHtml(html);
}

var table = htmldoc.DocumentNode.SelectSingleNode("//table[@class='table cotizacion']");

if (table != null)
{
    foreach (var moneda in monedasArg)
    {
        var nodo = table.SelectSingleNode($"tbody/tr/td[.='{moneda}']")?.ParentNode;
        var compra = nodo?.SelectSingleNode("td[2]")?.InnerText;
        var venta = nodo?.SelectSingleNode("td[3]")?.InnerText;

        if (compra != null && venta != null && decimal.TryParse(venta, NumberStyles.Any, culture, out var ventaDec))
        {
            Console.WriteLine($"{moneda}: {ventaDec:##.#######}");
        }
    } 
}